
function getTier(name)
  local laser = peripheral.wrap(name)
  return laser.getTier()
end

function getState(name)
  local laser = peripheral.wrap(name)
  return laser.state()
end
