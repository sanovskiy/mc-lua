os.loadAPI("/lib/debug")
os.loadAPI("/lib/warpdrive/MiningLaser")

print("Scanning network...")
local list = {}
local n = 1
local peripherals = peripheral.getNames()
for i = 1, #peripherals do
    if peripheral.getType(peripherals[i]) == "warpdriveMiningLaser" then
        n = n + 1
        table.insert(list, peripherals[i])
    elseif peripheral.getType(peripherals[i]) == "modem" then
        modem = peripheral.wrap(peripherals[i])
    elseif peripheral.getType(peripherals[i]) == "monitor" then
        monitor = peripheral.wrap(peripherals[i])
    end
end
print(tostring(#list) .. " miners found")

print(MiningLaser.getTier(list[1]))

statusString, isActive, energyLevel, currentLayer, mined, mineCount = MiningLaser.getState(list[1])
print('statusString :'..statusString)
print('energyLevel :'..energyLevel)
print('currentLayer :'..currentLayer)
print('mined :'..mined)
print('mineCount :'..mineCount)
